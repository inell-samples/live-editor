# live-editor

This is example of .NET Core **backend-service** and React **frontend-service** performing real-time collaboration files editing.

## Functional requirements

- The application offers a list of files stored on the server;
- A user can create a file;
- When a user picks one, allow the user to edit that file;
- The files will always be JSON files;
- Changes being made by any collaborator should be reflected immediately in all users' browsers who are looking at the same file;
- Users should be able to download a copy of the JSON file they are currently editing.

## Launching

1) Start backend 

   ```bash
   dotnet run --project ./live-editor-backend/live-editor-backend
   ```

2) Start frontend

   ```bash
   npm run dev --prefix ./live-editor-frontend/
   ```

3) Follow the URL provided in the output by `Vite`.

## Storing files

The following folder is mapped as files storage: `./live-editor-backend/live-editor-backend/Storage`.

The folder setting is awaliable in `appConfig.json` file:

```json
"FilesStorageDirectory": "Storage" 
```

