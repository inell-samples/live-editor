import { FormEvent, useEffect } from "react";
import { AppDispatch, LoadingState, RootState, useAppDispatch } from "../../store/store";
import { createFile, fetchFiles } from "../../store/filesSlice";
import { useSelector } from "react-redux";

export function CreateFileForm() {

    const dispatch = useAppDispatch();
    const { newFile: { data, status, error } } = useSelector((state: RootState) => state.files);
    const file = data?.data;

    useEffect(() => {
        // Reload files list after creating a new file
        if (status === LoadingState.Succeeded) {
            dispatch(fetchFiles());
        }
    }, [dispatch, status]);

    const submitDisabled = status === LoadingState.Pending;

    return (
        <form method="post" onSubmit={e => onSubmit(e, dispatch)}>
            <h2>Create file</h2>
            <input type="text" name="fileName" required maxLength={16} placeholder="New file name..." />
            <button type="submit" disabled={submitDisabled}>Create</button>
            {
                {
                    [LoadingState.Pending]: null,
                    [LoadingState.Failed]: <pre>{error}</pre>,
                    [LoadingState.Succeeded]: <pre>New file created: {file?.name}</pre>,
                    [LoadingState.Idle]: null,
                }[status]
            }
        </form>
    );
}

function onSubmit(event: FormEvent<HTMLFormElement>, dispatch: AppDispatch) {
    event.preventDefault();

    const form = event.target as HTMLFormElement;
    const data = new FormData(form);

    form.reset();

    dispatch(createFile(data));
}