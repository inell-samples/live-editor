import { UIEvent, useEffect, useRef } from "react";

import { WebSocketEndpoint, WebSocketConnectionStatus } from "../../features/WebSocketEndpoint";
import { WS_URL } from "../../const";
import style from './TextEditor.module.less';

export function TextEditor({ fileName, content }: {
    fileName: string,
    content: Blob | undefined
}) {
    const inputRef = useRef<HTMLTextAreaElement>(null);
    const backdropRef = useRef<HTMLDivElement>(null);
    const highlightsRef = useRef<HTMLDivElement>(null);

    const editorDisabled = content === undefined;

    const url = new URL(WS_URL);
    url.searchParams.append("fileName", fileName);
    const wse = new WebSocketEndpoint(url);
    wse.onMessage(wsMessageHandler);

    useEffect(() => {
        if (!content) {
            return;
        }

        connectWebSocketAsync()
            // Here user can be introduced
            //.then((ws) => ws.send("I'm connected"))
            .then(() => showContentAsync(content));

        window.addEventListener("beforeunload", () => wse.dicsonnect());

    }, [content]);

    async function showContentAsync(blob: Blob) {
        const textArea = inputRef.current!;
        textArea.value = await blob.text() ?? "";
        onChange({ target: textArea });
    }

    function onChange({ target }: { target: HTMLTextAreaElement }) {
        const text = target.value;
        const highlightedText = applyHighlights(text);
        highlightsRef.current!.innerHTML = highlightedText;
    }

    function onScroll(event: UIEvent<HTMLTextAreaElement>) {
        const target = event.target as HTMLTextAreaElement;
        backdropRef.current!.scrollTop = target.scrollTop;
    }

    // WebSockets

    function connectWebSocketAsync() {
        wse.socket?.readyState !== WebSocketConnectionStatus.Connecting && wse.connect();

        return new Promise<WebSocketEndpoint>((resolve) => {
            wse.onOpen(() => {
                resolve(wse);
            });
        });
    }

    function wsMessageHandler(this: WebSocket, event: MessageEvent) {
        wsPullChanges(event.data);
    }

    function wsPushChanges(value: string) {
        // Reconnect if nesessary
        const promise = wse.socket?.readyState !== WebSocketConnectionStatus.Open
            ? connectWebSocketAsync()
            : new Promise<WebSocketEndpoint>((resolve) => {
                resolve(wse);
            });

        promise.then(ws => ws.send(value));
    }

    function wsPullChanges(value: string) {
        console.log(value);

        const textArea = inputRef.current!;
        textArea.value = value;
        onChange({ target: textArea });
    }

    return (
        <form className={style.container}>
            <div ref={backdropRef} className={style.backdrop}>
                <div ref={highlightsRef} className={style.highlights}></div>
            </div>
            <textarea ref={inputRef} disabled={editorDisabled} className={style.textarea} onChange={(e) => { onChange(e); wsPushChanges(e.target.value); }} onScroll={onScroll} />
        </form>
    )
}

function applyHighlights(text: string) {
    return text
        // fixes the last new line
        .replace(/\n$/g, '\n\n');
}

