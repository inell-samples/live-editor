// Import reduxjs such way because it is not yet Node-ESM compatible
// https://github.com/reduxjs/redux-toolkit/issues/1960
import * as toolkitRaw from '@reduxjs/toolkit';
const { combineReducers, configureStore } = ((toolkitRaw as any).default ?? toolkitRaw) as typeof toolkitRaw;

import { useDispatch } from 'react-redux';
import filesSlice from './filesSlice';

console.debug("Redux dev tools active:", import.meta.env.DEV);

const rootReducer = combineReducers({
    files: filesSlice
});

const store = configureStore({
    reducer: rootReducer,
    devTools: import.meta.env.DEV
});

/**
 * Template literal type of appropriate Loading State values
 */
export enum LoadingState {
    Idle = 'idle',
    Pending = 'pending',
    Succeeded = 'succeeded',
    Failed = 'failed'
}

/**
 * Represents list of recuders of the current application
 */
export type RootState = ReturnType<typeof store.getState>;

/**
 * Wrapper of native `useDispatch` to have type corresponding
 */
export type AppDispatch = typeof store.dispatch;

/**
 * Use dispatch, which is typed for the current application
 */
export const useAppDispatch: () => AppDispatch = useDispatch;

export default store;