// Import reduxjs such way because it is not yet Node-ESM compatible
// https://github.com/reduxjs/redux-toolkit/issues/1960
import * as toolkitRaw from '@reduxjs/toolkit';
const { createSlice, createAsyncThunk } = ((toolkitRaw as any).default ?? toolkitRaw) as typeof toolkitRaw;

import { LoadingState } from './store';
import { API_URL, CREATE_URL } from '../const';
import { IFileResponse, IFilesListResponse } from '../models/DataModels';

export const fetchFiles = createAsyncThunk('files/fetch', async () => {
    const url = new URL(API_URL);

    const response = await fetch(url);
    return await response.json();
},);

export const createFile = createAsyncThunk('files/create', async (data: FormData) => {
    console.dir(...data);

    const url = new URL(CREATE_URL);

    const response = await fetch(url, {
        method: 'POST',
        body: data
    });
    return await response.json();
});

const filesSlice = createSlice({
    name: 'files',
    initialState: {
        filesList: {
            status: 'idle',
            data: {},
            error: null,
        },

        newFile: {
            status: 'idle',
            data: {},
            error: null,
        }
    } as SlicePartialState,
    reducers: {},
    extraReducers(builder) {
        buildCases(builder, fetchFiles, "filesList");
        buildCases(builder, createFile, "newFile");
    }
});

export default filesSlice.reducer;

function buildCases(
    builder: toolkitRaw.ActionReducerMapBuilder<SlicePartialState>, 
    func: toolkitRaw.AsyncThunk<any, any, AsyncThunkConfig>, 
    subStateName: SlicePartialStateKeys) {

    builder
        .addCase(func.pending, state => {
            const sub = state[subStateName];

            sub.status = LoadingState.Pending;
            sub.data = null;
            sub.error = null;
        })
        .addCase(func.fulfilled, (state, action) => {
            const sub = state[subStateName];

            sub.data = action.payload;
            sub.status = sub.data?.success
                ? LoadingState.Succeeded
                : LoadingState.Failed;
            sub.error = sub.data?.errorMessage;
        })
        .addCase(func.rejected, (state, action) => {
            const sub = state[subStateName];

            sub.status = LoadingState.Failed;
            sub.error = action.error.message;
        })
}

type AsyncThunkConfig = {};
type SlicePartialStateKeys = keyof SlicePartialState;
type SlicePartialState = {
    filesList: {
        status: LoadingState;
        data: IFilesListResponse | null;
        error: string | null | undefined;
    };
    newFile: {
        status: LoadingState;
        data: IFileResponse | null;
        error: string | null | undefined;
    };
}