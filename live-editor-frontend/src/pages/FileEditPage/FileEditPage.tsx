import { NavLink, useParams } from "react-router-dom";
import { useEffect, useState } from "react";

import { LoadingState } from "../../store/store";
import { OPEN_URL } from "../../const";
import { TextEditor } from "../../components/TextEditor/TextEditor";

export function FileEditPage() {
    const { fileName } = useParams();

    const [status, setStatus] = useState(LoadingState.Idle);
    const [blobContent, setBlobContent] = useState<Blob>();

    useEffect(() => {
        if (!fileName) {
            return;
        }

        openFileAsync(fileName)
            .then(setBlobContent)
            .catch(() => setStatus(LoadingState.Failed))
    }, [fileName]);

    async function openFileAsync(fileName: string): Promise<Blob> {
        setStatus(LoadingState.Pending);

        const url = new URL(OPEN_URL);
        url.searchParams.append("name", fileName);

        const response = await fetch(url);
        if (!response || response.status != 200) {
            setStatus(LoadingState.Failed);
            return Promise.reject();
        }

        setStatus(LoadingState.Succeeded);
        return await response.blob();
    }

    async function downloadFile(fileName: string | undefined) {
        if (!fileName) {
            return;
        }

        const blob = await openFileAsync(fileName);
        // Convert your blob into a Blob URL (a special url that points to an object in the browser's memory)
        const blobUrl = URL.createObjectURL(blob);

        const link = document.createElement("a");
        link.href = blobUrl;
        link.download = fileName;

        document.body.appendChild(link);

        link.dispatchEvent(
            new MouseEvent('click', {
                bubbles: true,
                cancelable: true,
                view: window
            })
        );

        document.body.removeChild(link);
    }

    return (
        <section>
            <NavLink to={`/files`} reloadDocument={true}>Files list</NavLink>
            <h1>Edit file</h1>
            <h2>{fileName}</h2>
            <button onClick={() => downloadFile(fileName)}>Download</button>
            {status === LoadingState.Failed && <pre>Problem while loading the file</pre>}
            <TextEditor fileName={fileName!} content={blobContent} />
        </section>
    );
}