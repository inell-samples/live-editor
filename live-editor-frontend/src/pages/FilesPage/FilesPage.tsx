import { LoadingState, RootState, useAppDispatch } from "../../store/store";
import { fetchFiles } from "../../store/filesSlice";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";

import { IFileInfoDto } from "../../models/DataModels";
import { CreateFileForm } from "../../components/CreateFileForm/CreateFileForm";

export function FilesPage() {

    const dispatch = useAppDispatch();
    const { filesList: { data, status } } = useSelector((state: RootState) => state.files);

    const files = data?.data;

    useEffect(() => {
        dispatch(fetchFiles());
    }, [dispatch]);

    return (
        <section>
            <h1>Files</h1>
            <CreateFileForm />
            <article>
                <h2>Storage files list</h2>
                {
                    {
                        [LoadingState.Pending]: <pre>Loading data...</pre>,
                        [LoadingState.Failed]: <pre>Failed to feth results: {data?.errorMessage ?? "No backend response"}</pre>,
                        [LoadingState.Succeeded]: <>
                            {
                                files?.length !== 0
                                    ? <ul>
                                        {files?.map((item: IFileInfoDto) => <FileInfoItem key={item.name} file={item} />)}
                                    </ul>
                                    : <pre>There are no files. Pls, create a new one</pre>
                            }
                        </>,
                        [LoadingState.Idle]: null,
                    }[status]
                }
            </article>
        </section>
    );
}

function FileInfoItem({ file }: { file: IFileInfoDto }) {
    const date = new Date(file.changeDateTime);

    return (
        <li>
            <NavLink to={`/files/edit/${encodeURI(file.name)}`}>{file.name}</NavLink>
            {date.toLocaleString()}
        </li>
    );
}