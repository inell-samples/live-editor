interface IResultResponse {
    success: boolean;
    errorMessage: string | null;
}

export interface IFilesListResponse extends IResultResponse {
    data: Array<IFileInfoDto>;
    total: number;
}

export interface IFileResponse extends IResultResponse {
    data: IFileInfoDto;
}

export interface IFileInfoDto {
    name: string;
    changeDateTime: string;    
}