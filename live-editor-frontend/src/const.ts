export const API_URL = "http://localhost:5131/api/files";
export const CREATE_URL = `${API_URL}/create`;
export const OPEN_URL = `${API_URL}/open`;
export const WS_URL = 'ws://localhost:5131/ws';