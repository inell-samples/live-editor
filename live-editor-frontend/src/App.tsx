import { Route, RouterProvider, createBrowserRouter, createRoutesFromElements, Outlet, useNavigate } from "react-router-dom";
import { FilesPage } from "./pages/FilesPage/FilesPage";
import { useEffect } from "react";
import { Provider } from 'react-redux';

import store from './store/store';
import { FileEditPage } from "./pages/FileEditPage/FileEditPage";

const router = createBrowserRouter(
    createRoutesFromElements(
        <Route path="/" element={<Root />}>
            <Route index element={<Index />} />
            <Route path="files" element={<FilesPage />} />
            <Route path="files/edit/:fileName" element={<FileEditPage />} />
            <Route path="*" element={<Error />} />
        </Route>
    )
);

function Root() {
    return (
        <>
            <p>[logo]</p>
            <Outlet />
        </>
    )
}

function Index() {
    const navigate = useNavigate();

    // Redirect to /files location
    useEffect(() => {
        navigate('/files');
    }, [navigate]);

    return <></>;
}

function Error() {
    return (
        <b>Error page</b>
    )
}

export default function App() {
    return (
        <Provider store={store}>
            <RouterProvider router={router}></RouterProvider>
        </Provider>
    )
};