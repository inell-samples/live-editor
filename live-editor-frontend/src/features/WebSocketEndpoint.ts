type WebSocketEventName = 'onopen' | 'onclose' | 'onmessage' | 'onerror';

type WebSocketEventHandler = (this: WebSocket, event: Event) => void;

export enum WebSocketConnectionStatus {
    Connecting = 0,
    Open = 1,
    Closing = 2,
    Closed = 3
}

/**
 * Wraps javascript `WebSocket` object
 */
export class WebSocketEndpoint {

    readonly url: string | URL | undefined = undefined;

    get socket() { return this._socket; }
    private _socket: WebSocket | null = null;

    private readonly _handlers = new Map<WebSocketEventName, WebSocketEventHandler>();

    constructor(url: string | URL) {
        this.url = url;
    }

    onOpen(eventHandler: (this: WebSocket, event: Event) => void) {
        this.updateEventHandler('onopen', eventHandler);
    }

    onClose(eventHandler: (this: WebSocket, event: Event) => void) {
        this.updateEventHandler('onclose', eventHandler);
    }

    onMessage(eventHandler: (this: WebSocket, event: MessageEvent<any>) => void) {
        this.updateEventHandler('onmessage', eventHandler as WebSocketEventHandler);
    }

    onError(eventHandler: (this: WebSocket, event: Event) => void) {
        this.updateEventHandler('onerror', eventHandler);
    }

    private updateEventHandler(name: WebSocketEventName, eventHandler: WebSocketEventHandler) {
        this._handlers.set(name, eventHandler);
        if (this._socket) {
            this._socket[name] = this._handlers.get(name)!;
        }
    }

    connect() {
        /*if (this._socket)
            throw Error('The socket is already connected');*/

        this._socket = new WebSocket(this.url!);

        // Attach event-handlers to the new WebSocket
        ["onopen", "onclose", "onmessage", "onerror"].forEach(n => {
            this._socket![n as WebSocketEventName] = this._handlers.get(n as WebSocketEventName) ?? null;
        });
    }

    dicsonnect() {
        if (!this._socket)
            throw Error('The socket is not yet connected');

        this._socket.close();
        this._socket = null;
    }

    send(message: string) {
        if (!this._socket)
            throw Error('The socket is not yet connected');

        if (this._socket.readyState !== WebSocketConnectionStatus.Open)
            throw Error(`The socket is not opened, it has state: ${WebSocketConnectionStatus[this._socket.readyState]}`);

        this._socket.send(message);
    }
}