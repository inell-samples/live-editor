using LiveEditor.Configuration;
using LiveEditor.Services;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
{
    var services = builder.Services;
    var config = builder.Configuration;

    services.AddCors();

    services.AddOptions<RootOptions>()
        .Bind(config)
        .ValidateDataAnnotations()
        .ValidateOnStart();

    services.AddControllers();

    services.AddScoped<IFilesService, FilesService>();
}

var app = builder.Build();

// Configure the HTTP request pipeline.
{
    app.UseCors(options =>
    {
        options.AllowAnyOrigin();
    });

    app.MapControllers();
    app.UseWebSockets();
}

app.Run();