﻿using LiveEditor.Models;
using LiveEditor.Models.Dto;
using LiveEditor.Services;
using Microsoft.AspNetCore.Mvc;

namespace live_editor_backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FilesController : ControllerBase
    {
        private readonly IFilesService _fileService;

        public FilesController(IFilesService fileService)
        {
            _fileService = fileService;
        }

        [HttpGet]
        public FilesListResponse Get()
        {
            var files = _fileService.GetFiles().Select(x => new FileInfoDto(x));

            var response = new FilesListResponse
            {
                Data = files,
                Total = files.Count()
            };

            return response;
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> Open(string name)
        {
            var blob = await _fileService.OpenFile(name);

            return blob != null
                ? File(blob, "application/json")
                : NotFound(name);
        }

        [HttpPost("[action]")]
        public ActionResult<FileResponse> Create([FromForm] string fileName)
        {
            var response = new FileResponse();
            FileInfo? file;

            try
            {
                file = _fileService.CreateFile(fileName);
                response.Data = new FileInfoDto(file);
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorMessage = ex.Message;

                return BadRequest(response);
            }

            return CreatedAtAction(nameof(Open), new { name = file.Name }, response);
        }
    }
}
