﻿using LiveEditor.Services;
using Microsoft.AspNetCore.Mvc;
using System.Net.WebSockets;
using System.Text;

namespace LiveEditor.Controllers;

public class WebSocketController : ControllerBase
{
    private static List<KeyValuePair<string, WebSocket>> _connections = new List<KeyValuePair<string, WebSocket>>();
    private readonly IFilesService _fileService;

    public WebSocketController(IFilesService fileService)
    {
        _fileService = fileService;
    }

    [Route("/ws")]
    public async Task Get()
    {
        if (HttpContext.WebSockets.IsWebSocketRequest)
        {
            var fileName = HttpContext.Request.Query["fileName"];
            using var ws = await HttpContext.WebSockets.AcceptWebSocketAsync();

            // Add client to collection
            _connections.Add(new KeyValuePair<string, WebSocket>(fileName, ws));

            await RecieveMessageAsync(ws, this.OnMessageHandler);
        }
        else
        {
            HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
        }
    }

    private static async Task RecieveMessageAsync(WebSocket ws, Func<WebSocket, WebSocketReceiveResult, byte[], Task> handleMessage)
    {
        var buffer = new byte[1024 * 4];

        while(ws.State == WebSocketState.Open)
        {
            var result = await ws.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);

            if (result.MessageType == WebSocketMessageType.Close)
            {
                // Remove client from collection
                await ws.CloseAsync(result.CloseStatus ?? WebSocketCloseStatus.Empty, result.CloseStatusDescription, CancellationToken.None);
            }

            await handleMessage(ws, result, buffer);
        }
    }

    private async Task OnMessageHandler(WebSocket ws, WebSocketReceiveResult result, byte[] buffer)
    {
        if (result.MessageType == WebSocketMessageType.Text)
        {
            var message = Encoding.UTF8.GetString(buffer, 0, result.Count);

            var fileName = _connections.First(x => x.Value == ws).Key;
            await Broadcast(ws, fileName, message);

            await _fileService.WriteToFile(fileName, message);
        }
    }

    private static async Task Broadcast(WebSocket ws, string fileName, string message)
    {
        var bytes = Encoding.UTF8.GetBytes(message);
        var arraySegment = new ArraySegment<byte>(bytes, 0, bytes.Length);

        var recievers = _connections.Where(x => x.Key == fileName && x.Value != ws && x.Value.State == WebSocketState.Open);
        foreach (var reciever in recievers)
        {
            await reciever.Value.SendAsync(arraySegment, WebSocketMessageType.Text, endOfMessage: true, CancellationToken.None);
        }
    }
}
