﻿using System.ComponentModel.DataAnnotations;

namespace LiveEditor.Configuration;

public record RootOptions
{
    [Required]
    public required string FilesStorageDirectory { get; set; }
}
