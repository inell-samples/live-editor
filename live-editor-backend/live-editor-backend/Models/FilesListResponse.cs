﻿using LiveEditor.Models.Dto;

namespace LiveEditor.Models;

public record FilesListResponse : ResultResponse
{
    public IEnumerable<FileInfoDto> Data { get; set; } = Array.Empty<FileInfoDto>();
    public int Total { get; set; } = 0;
}
