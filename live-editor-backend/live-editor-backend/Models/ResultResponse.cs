﻿namespace LiveEditor.Models;

public record ResultResponse
{
    public bool Success { get; set; } = true;
    public string? ErrorMessage { get; set; }
}
