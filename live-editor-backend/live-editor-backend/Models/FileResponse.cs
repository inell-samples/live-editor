﻿using LiveEditor.Models.Dto;

namespace LiveEditor.Models;

public record FileResponse : ResultResponse
{
    public FileInfoDto? Data { get; set; }
}
