﻿namespace LiveEditor.Models.Dto;

public record FileInfoDto
{
    public string Name { get; set; }
    public DateTime ChangeDateTime { get; set; }

    public FileInfoDto(FileInfo fileInfo)
    {
        Name = fileInfo.Name;
        ChangeDateTime = fileInfo.LastWriteTime;
    }
}
