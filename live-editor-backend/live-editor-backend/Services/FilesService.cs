﻿using LiveEditor.Configuration;
using Microsoft.Extensions.Options;
using System.IO;

namespace LiveEditor.Services;

public interface IFilesService
{
    public IEnumerable<FileInfo> GetFiles();
    public FileInfo CreateFile(string name);
    public Task<byte[]?> OpenFile(string name);
    public Task WriteToFile(string name, string content);
}

public class FilesService : IFilesService
{
    private readonly string _path;

    public FilesService(IOptions<RootOptions> options)
    {
        _path = Path.Combine(Directory.GetCurrentDirectory(), options.Value.FilesStorageDirectory);
    }

    public FileInfo CreateFile(string name)
    {
        var filePath = Path.Combine(_path, $"{name}.json");

        using StreamWriter outputFile = new(filePath, append: false);
        outputFile.WriteLine("{}");

        return new FileInfo(filePath);
    }

    public IEnumerable<FileInfo> GetFiles()
    {
        var files = new DirectoryInfo(_path).GetFiles();

        return files;
    }

    public async Task<byte[]?> OpenFile(string name)
    {
        var filePath = Path.Combine(_path, name);

        if (!File.Exists(filePath))
        {
            return null;
        }

        var bytes = await File.ReadAllBytesAsync(filePath);

        return bytes;
    }

    public async Task WriteToFile(string name, string content)
    {
        var filePath = Path.Combine(_path, name);

        if (!File.Exists(filePath))
        {
            return;
        }

        // For simplicity locks are not covered
        await File.WriteAllTextAsync(filePath, content);
    }
}
